import shutil
import logging
import config
import os

import sh

from utils import random_string_generator, set_logging


__author__ = 'fomars'


class Bot(object):
    GIT_PATH = '.git/'
    CONFIG_PATH = '.git/config'

    def __init__(self, repo_url, file, email, repo_name='repo', jira_ticket='', clean_up=True, username='bot'):
        logging.info('Initializing Bot')
        self.repo_name = repo_name
        self.repo_path = os.path.join(os.getcwd(), repo_name)
        self.repo_url = repo_url
        self.filename = file
        self.jira_ticket = jira_ticket
        self.clean = clean_up
        self.username = username
        self.email = email

    def run(self):
        self.checkout()
        self.add_lines()
        self.check_in()
        if self.clean:
            self.clean_up()

    def checkout(self):
        logging.info('Check out the repository')
        if os.path.exists(self.repo_name):
            logging.info('The repository already exists, fetching last changes')
            sh.cd(self.repo_name)
            # git fetch --all
            # git reset --hard origin/master
            sh.git.fetch('--all')
            sh.git.reset('--hard', 'origin/master')
            sh.cd('../')
        else:
            sh.git.clone(self.repo_url, self.repo_name)
        assert os.path.exists(os.path.join(self.repo_name, self.GIT_PATH))

    def clean_up(self):
        logging.info('Cleaning up')
        shutil.rmtree(self.repo_path)

    def add_lines(self):
        with open(os.path.join(self.repo_name, self.filename), 'a') as file:
            s = '\nYet another commit:\n\t' + random_string_generator(30)
            logging.info('Writing to file {0:s} string "{1:s}"'.format(self.filename, s))
            file.writelines([s])

    def check_in(self):
        logging.info('Preparing check in')
        sh.cd(self.repo_name)
        logging.info('Setting username {0:s}, email {1:s}'.format(self.username, self.email))
        sh.git.config('user.name', self.username)
        sh.git.config('user.email', self.email)
        sh.git.add('.')
        sh.git.commit('-a', '-m', self.jira_ticket + ' yet another dummy commit')
        logging.info('Pushing to repository')
        output = sh.git.push()
        if 0 == output.exit_code:
            logging.info('Success')
        else:
            logging.error(output)
        sh.cd('../')


if __name__ == '__main__':
    set_logging()
    Bot(repo_url=config.REPO_URL, file=config.FILE_TO_EDIT, email=config.GIT_EMAIL, repo_name=config.REPO_NAME,
        jira_ticket=config.JIRA_TICKET, clean_up=config.CLEAN_UP, username=config.GIT_USERNAME).run()